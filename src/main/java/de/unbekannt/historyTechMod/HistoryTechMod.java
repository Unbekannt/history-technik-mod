package de.unbekannt.historyTechMod;

import de.unbekannt.historyTechMod.init.HistoryTechnikModInt;
import net.minecraftforge.fml.common.Mod;


@Mod(HistoryTechMod.MODID)
public class HistoryTechMod {
    public static final String MODID = "historytechnikmod";

    public HistoryTechMod(){
        HistoryTechnikModInt.init();
    }
}
