package de.unbekannt.historyTechMod.init;

import de.unbekannt.historyTechMod.ageless.AgelessInit;
import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import de.unbekannt.historyTechMod.sedentism.SedentismClientInit;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

public class HistoryTechnikModInt {
    public static void init(){
        commonInit();
        sendentismInit();
        metalAgeInit();

    }

    private static void metalAgeInit() {
        MetalAgeInit.init();
    }

    private static void commonInit(){
        AgelessInit.init();
    }

    private static void sendentismInit(){
        SedentismInit.init();
        FMLJavaModLoadingContext.get().getModEventBus().addListener(SedentismClientInit::init);
    }
}
