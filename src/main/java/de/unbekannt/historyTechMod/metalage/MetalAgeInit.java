package de.unbekannt.historyTechMod.metalage;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace.EarlyAlloyFurnaceInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceInit;
import de.unbekannt.historyTechMod.metalage.blocks.ore.CopperOre;
import de.unbekannt.historyTechMod.metalage.blocks.ore.TinOre;
import de.unbekannt.historyTechMod.metalage.items.tools.CopperPickAxe;
import net.minecraft.block.Block;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class MetalAgeInit {

    public static final String NAMEPREFIX = "metalage/";

    public static void init(){
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        TILEENTITYS.register(FMLJavaModLoadingContext.get().getModEventBus());
        CONTAINERTYPES.register(FMLJavaModLoadingContext.get().getModEventBus());
        new EarlyFurnaceInit();
        new EarlyAlloyFurnaceInit();
    }

    public static final DeferredRegister<Block> BLOCKS =
            DeferredRegister.create(ForgeRegistries.BLOCKS, HistoryTechMod.MODID);

    public static final DeferredRegister<Item> ITEMS =
            DeferredRegister.create(ForgeRegistries.ITEMS, HistoryTechMod.MODID);

    public static final DeferredRegister<TileEntityType<?>> TILEENTITYS =
            DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, HistoryTechMod.MODID);

    public static final DeferredRegister<ContainerType<?>> CONTAINERTYPES =
            DeferredRegister.create(ForgeRegistries.CONTAINERS, HistoryTechMod.MODID);

    public static final RegistryObject<Block> TIN_ORE =
            BLOCKS.register(NAMEPREFIX+"tin_ore", TinOre::new);

    public static final RegistryObject<Item> TIN_ORE_ITEM =
            ITEMS.register(NAMEPREFIX+"tin_ore", () -> new BlockItem(
                    TIN_ORE.get(),
                    new Item.Properties().group(ModItemGroups.METALAGEGROUP)
            ));

    public static final RegistryObject<Item> TIN_INGOT =
            ITEMS.register(NAMEPREFIX+"tin_ingot",
                    () -> new Item(new Item.Properties().group(ModItemGroups.METALAGEGROUP)));

    public static final RegistryObject<Block> COPPER_ORE =
            BLOCKS.register(NAMEPREFIX+"copper_ore", CopperOre::new);

    public static final RegistryObject<Item> COPPER_ORE_ITEM =
            ITEMS.register(NAMEPREFIX+"copper_ore", () -> new BlockItem(
                    COPPER_ORE.get(),
                    new Item.Properties().group(ModItemGroups.METALAGEGROUP)
            ));

    public static final RegistryObject<Item> COPPER_INGOT = ITEMS.register(NAMEPREFIX+"copper_ingot",
            () -> new Item(new Item.Properties().group(ModItemGroups.METALAGEGROUP)));

    public static final RegistryObject<Item> COPPER_PICKAXE =
            ITEMS.register(NAMEPREFIX+"copper_pickaxe", CopperPickAxe::new);

    public static final RegistryObject<Item> BRONZE_TONG =
            ITEMS.register(NAMEPREFIX+"bronze_tong",
                    () -> new Item(new Item.Properties().group(ModItemGroups.METALAGEGROUP)));

    public static final RegistryObject<Item> BRONZE_INGOT =
            ITEMS.register(NAMEPREFIX+"bronze_ingot",
                    () -> new Item(new Item.Properties().group(ModItemGroups.METALAGEGROUP)));



}
