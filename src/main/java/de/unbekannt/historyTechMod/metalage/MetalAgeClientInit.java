package de.unbekannt.historyTechMod.metalage;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace.EarlyAllowFurnaceScreen;
import de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace.EarlyAlloyFurnaceInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceScreen;
import net.minecraft.client.gui.ScreenManager;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = HistoryTechMod.MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
public class MetalAgeClientInit {

    @SubscribeEvent
    public static void init(final FMLClientSetupEvent event){
        ScreenManager.registerFactory(EarlyFurnaceInit.earlyFurnaceContainer.get(), EarlyFurnaceScreen::new);
        ScreenManager.registerFactory(EarlyAlloyFurnaceInit.earlyAlloyFurnaceContainer.get(), EarlyAllowFurnaceScreen::new);
    }
}