package de.unbekannt.historyTechMod.metalage.items.tools;

import de.unbekannt.historyTechMod.ageless.ItemTiers;
import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import net.minecraft.item.Item;
import net.minecraft.item.PickaxeItem;

public class CopperPickAxe extends PickaxeItem {


    public CopperPickAxe() {
        super(ItemTiers.COPPER, 1, -3f, new Item.Properties().group(ModItemGroups.METALAGEGROUP));
    }
}
