package de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace;

import de.unbekannt.historyTechMod.ageless.AgelessInit;
import de.unbekannt.historyTechMod.ageless.recipies.earlyFurnace.EarlyFurnaceRecipe;
import de.unbekannt.historyTechMod.metalage.blocks.commonEarlyMachine.CommonEarlyMachineTileEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RecipeWrapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

public class EarlyFurnaceTileEntity extends CommonEarlyMachineTileEntity<EarlyFurnaceRecipe> {

    public static final String SCREENNAME = "screen.historytechmod.metalage.earlyFurnace";


    public EarlyFurnaceTileEntity() {
        super(EarlyFurnaceInit.earlyFurnaceTileEntity.get(), AgelessInit.EARLY_FURNACE_TYPE);
    }

    @Override
    protected ItemStackHandler createHandler() {
        return new ItemStackHandler(3) {

            @Override
            protected void onContentsChanged(int slot) {
                // To make sure the TE persists when the chunk is saved later we need to
                // mark it dirty every time the item handler changes
                markDirty();
            }

            @Override
            public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                if(slot == 0 ){
                    return EarlyFurnaceTileEntity.getAllRecipeInputsAsItems(AgelessInit.EARLY_FURNACE_TYPE, world).contains(stack.getItem());
                }
                if (slot == 1) return ForgeHooks.getBurnTime(stack) > 0;
                //if (slot == 2) return EarlyFurnaceTileEntity.findRecipeByType(AgelessInit.EARLY_FURNACE_TYPE).stream().anyMatch(iRecipe -> iRecipe.getRecipeOutput() == stack);
                return false;
            }

            @Nonnull
            @Override
            public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                if (slot == 0 && !EarlyFurnaceTileEntity.getAllRecipeInputsAsItems(AgelessInit.EARLY_FURNACE_TYPE, world).contains(stack.getItem())) {
                    return stack;
                }
                if(slot == 1 && !(ForgeHooks.getBurnTime(stack) > 0)){
                    return stack;
                }

                return super.insertItem(slot, stack, simulate);
            }
        };
    }



    @Override
    protected EarlyFurnaceRecipe matching(Set<IRecipe<?>> recipes, RecipeWrapper wrapper, World world) {
        for (IRecipe<?> iRecipe : recipes){
            if(! (iRecipe instanceof EarlyFurnaceRecipe)) continue;
            EarlyFurnaceRecipe recipe = (EarlyFurnaceRecipe) iRecipe;
            if(recipe.matches(wrapper, world)){
                return recipe;
            }
        }
        return null;
    }

    @Override
    protected String getScreenName() {
        return SCREENNAME;
    }

    @Nullable
    @Override
    public Container createMenu(int p_createMenu_1_, PlayerInventory p_createMenu_2_, PlayerEntity p_createMenu_3_) {
        assert world != null;
        return new EarlyFurnaceContainer(p_createMenu_1_, this.world, this.pos, p_createMenu_2_, p_createMenu_3_, teData);
    }
}
