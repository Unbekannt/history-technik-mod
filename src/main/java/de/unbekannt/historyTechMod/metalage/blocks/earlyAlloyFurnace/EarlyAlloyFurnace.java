package de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace;

import de.unbekannt.historyTechMod.metalage.blocks.commonEarlyMachine.CommonEarlyMachineBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;
import org.jetbrains.annotations.Nullable;

public class EarlyAlloyFurnace extends CommonEarlyMachineBlock {

    public EarlyAlloyFurnace() {
        super(Properties.from(Blocks.FURNACE));
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return null;
    }
}
