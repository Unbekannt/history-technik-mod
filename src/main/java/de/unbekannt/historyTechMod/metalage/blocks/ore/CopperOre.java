package de.unbekannt.historyTechMod.metalage.blocks.ore;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Blocks;
import net.minecraft.block.OreBlock;

public class CopperOre extends OreBlock {
    public CopperOre() {
        super(AbstractBlock.Properties.from(Blocks.STONE));
    }
}
