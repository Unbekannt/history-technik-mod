package de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace;


import de.unbekannt.historyTechMod.metalage.blocks.commonEarlyMachine.CommonEarlyMachineBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;

import javax.annotation.Nullable;

public class EarlyFurnace extends CommonEarlyMachineBlock {

    public EarlyFurnace() {
        super(Properties.from(Blocks.FURNACE)); //from(Blocks.FURNACE)
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new EarlyFurnaceTileEntity();
    }
}
