package de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace;

import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import net.minecraft.block.Block;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.fml.RegistryObject;

import static de.unbekannt.historyTechMod.metalage.MetalAgeInit.*;

public class EarlyAlloyFurnaceInit {
    public static final RegistryObject<Block> earlyAlloyFurnace =
            BLOCKS.register(NAMEPREFIX+"early_alloy_furnace", EarlyAlloyFurnace::new);

    public static final RegistryObject<Item> earlyAlloyFurnaceItem =
            ITEMS.register(NAMEPREFIX+"early_alloy_furnace", () -> new BlockItem(
                    earlyAlloyFurnace.get(),
                    new Item.Properties().group(ModItemGroups.METALAGEGROUP)
            ));

    public static final RegistryObject<TileEntityType<EarlyAlloyFurnaceTileEntity>> earlyAlloyFurnaceTileEntity =
            TILEENTITYS.register(NAMEPREFIX+"early_alloy_furnace",
                    () -> TileEntityType.Builder
                            .create(EarlyAlloyFurnaceTileEntity::new, earlyAlloyFurnace.get())
                            .build(null));

    public static final RegistryObject<ContainerType<EarlyAlloyFurnaceContainer>> earlyAlloyFurnaceContainer =
            CONTAINERTYPES.register(NAMEPREFIX+"early_alloy_furnace", () -> IForgeContainerType.create(
                    EarlyAlloyFurnaceContainer::new));
}
