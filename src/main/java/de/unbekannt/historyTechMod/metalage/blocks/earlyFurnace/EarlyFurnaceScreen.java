package de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace;

import de.unbekannt.historyTechMod.metalage.blocks.commonEarlyMachine.CommonEarlyMachineScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class EarlyFurnaceScreen extends CommonEarlyMachineScreen<EarlyFurnaceContainer> {

    private ResourceLocation GUI = new ResourceLocation("minecraft", "textures/gui/container/furnace.png");

    public EarlyFurnaceScreen(EarlyFurnaceContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
        //xSize = 50;
        //ySize = 50;
    }

    @Override
    protected ResourceLocation getGUI() {
        return GUI;
    }
}
