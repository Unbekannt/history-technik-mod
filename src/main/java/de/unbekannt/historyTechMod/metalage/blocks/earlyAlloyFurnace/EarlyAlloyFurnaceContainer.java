package de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace;

import de.unbekannt.historyTechMod.metalage.blocks.commonEarlyMachine.CommonEarlyMachineContainer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IIntArray;
import net.minecraft.util.IntArray;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class EarlyAlloyFurnaceContainer extends CommonEarlyMachineContainer {

    public EarlyAlloyFurnaceContainer(int windowId, PlayerInventory playerInventory, PacketBuffer extraData) {
        this(windowId,playerInventory.player.world, extraData.readBlockPos(), playerInventory, playerInventory.player, new IntArray(4));
    }


    protected EarlyAlloyFurnaceContainer(int id, World world, BlockPos pos, PlayerInventory playerInventoryIn, PlayerEntity player, IIntArray furnaceData) {
        super(EarlyAlloyFurnaceInit.earlyAlloyFurnaceContainer.get(), id, world, pos, playerInventoryIn, player, EarlyAlloyFurnaceInit.earlyAlloyFurnace.get(), furnaceData);
    }

    @Override
    protected void addSlots() {

    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        return null;
    }
}
