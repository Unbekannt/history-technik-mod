package de.unbekannt.historyTechMod.metalage.blocks.commonEarlyMachine;

import de.unbekannt.historyTechMod.ageless.utils.CommonContainer;
import net.minecraft.block.Block;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.IIntArray;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import org.jetbrains.annotations.Nullable;

public abstract class CommonEarlyMachineContainer extends CommonContainer {


    private final IIntArray furnaceData;

    protected CommonEarlyMachineContainer(@Nullable ContainerType<?> type, int id, World world, BlockPos pos, PlayerInventory playerInventoryIn, PlayerEntity player, Block blocktype, IIntArray data) {
        super(type, id, world, pos, playerInventoryIn, player, blocktype);

        this.furnaceData = data;

        trackIntArray(furnaceData);
    }



    public int getCookProgressionScaled() {
        int i = furnaceData.get(1);
        int j = furnaceData.get(2);
        return j != 0 && i != 0 ? i * 24 / j : 0;
    }


    public int getBurnLeftScaled() {
        int i = furnaceData.get(3);
        if (i == 0) {
            i = 200;
        }

        return furnaceData.get(0) * 13 / i;
    }


    public boolean isBurning() {
        return furnaceData.get(0) > 0;
    }

}
