package de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.metalage.blocks.commonEarlyMachine.CommonEarlyMachineScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import org.jetbrains.annotations.NotNull;

public class EarlyAllowFurnaceScreen extends CommonEarlyMachineScreen<EarlyAlloyFurnaceContainer> {

    private ResourceLocation GUI = new ResourceLocation(HistoryTechMod.MODID, "textures/gui/metalage/alloy_furnace.png");

    public EarlyAllowFurnaceScreen(EarlyAlloyFurnaceContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
    }

    @NotNull
    @Override
    protected ResourceLocation getGUI() {
        return GUI;
    }
}
