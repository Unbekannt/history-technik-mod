package de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace;

import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import net.minecraft.block.Block;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.fml.RegistryObject;

import static de.unbekannt.historyTechMod.metalage.MetalAgeInit.*;

public class EarlyFurnaceInit {
    public static final RegistryObject<Block> earlyFurnace =
            BLOCKS.register(NAMEPREFIX+"early_furnace", EarlyFurnace::new);

    public static final RegistryObject<Item> earlyFurnaceItem =
            ITEMS.register(NAMEPREFIX+"early_furnace", () -> new BlockItem(
                    earlyFurnace.get(),
                    new Item.Properties().group(ModItemGroups.METALAGEGROUP)
            ));

    public static final RegistryObject<TileEntityType<EarlyFurnaceTileEntity>> earlyFurnaceTileEntity =
            TILEENTITYS.register(NAMEPREFIX+"early_furnace",
                    () -> TileEntityType.Builder
                            .create(EarlyFurnaceTileEntity::new, earlyFurnace.get())
                            .build(null));

    public static final RegistryObject<ContainerType<EarlyFurnaceContainer>> earlyFurnaceContainer =
            CONTAINERTYPES.register(NAMEPREFIX+"early_furnace", () -> IForgeContainerType.create(
                    EarlyFurnaceContainer::new));
}
