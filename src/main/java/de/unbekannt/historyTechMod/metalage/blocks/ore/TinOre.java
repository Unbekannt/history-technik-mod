package de.unbekannt.historyTechMod.metalage.blocks.ore;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Blocks;
import net.minecraft.block.OreBlock;

public class TinOre extends OreBlock {

    public TinOre() {
        super(AbstractBlock.Properties.from(Blocks.STONE));
    }
}
