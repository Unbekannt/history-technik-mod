package de.unbekannt.historyTechMod.sedentism;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernInit;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernScreen;
import net.minecraft.client.gui.ScreenManager;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = HistoryTechMod.MODID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)

public class SedentismClientInit {


    public static void init(final FMLClientSetupEvent event){
        ScreenManager.registerFactory(QuernInit.QUERNCONTAINER.get(), QuernScreen::new);
    }
}
