package de.unbekannt.historyTechMod.sedentism;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import de.unbekannt.historyTechMod.sedentism.blocks.ChiseledStone;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernInit;
import de.unbekannt.historyTechMod.sedentism.items.Chisel;
import de.unbekannt.historyTechMod.sedentism.items.Flail;
import de.unbekannt.historyTechMod.sedentism.items.StickAndGrass;
import de.unbekannt.historyTechMod.sedentism.items.StoneHammer;
import net.minecraft.block.Block;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class SedentismInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, HistoryTechMod.MODID);

    public static final DeferredRegister<Item> BLOCKITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, HistoryTechMod.MODID);

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, HistoryTechMod.MODID);

    public static final DeferredRegister<ContainerType<?>> CONTAINERTYPES = DeferredRegister.create(ForgeRegistries.CONTAINERS, HistoryTechMod.MODID);

    public static final DeferredRegister<TileEntityType<?>> TILEENTITYS =
            DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, HistoryTechMod.MODID);

    public static void init(){
        new QuernInit();
        ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        BLOCKITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        TILEENTITYS.register(FMLJavaModLoadingContext.get().getModEventBus());
        CONTAINERTYPES.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<Item> STONE_HAMMER = ITEMS.register("sedentism/stone_hammer", StoneHammer::new);

    public static final RegistryObject<Item> CHISEL = ITEMS.register("sedentism/chisel", Chisel::new);

    public static final RegistryObject<Item> STONEEDGE = ITEMS.register("sedentism/stone_edge",
            () -> new Item(new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));

    public static final RegistryObject<Item> STONECENTER = ITEMS.register("sedentism/stone_center",
            () -> new Item(new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));

    public static final RegistryObject<Item> HANDSTONE = ITEMS.register("sedentism/hand_stone",
            () -> new Item(new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));

    public static final RegistryObject<Item> BRANCH = ITEMS.register("sedentism/branch",
            () -> new Item(new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));

    public static final RegistryObject<Item> FLAIL = ITEMS.register("sedentism/flail",
            Flail::new);

    public static final RegistryObject<Item> GRAIN = ITEMS.register("sedentism/wheat_grain",
            () -> new Item(new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));

    public static final RegistryObject<Item> FLOUR = ITEMS.register("sedentism/flour",
            () -> new Item(new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));

    public static final RegistryObject<Item> DRY_GRASS = ITEMS.register("sedentism/dry_gras",
            () -> new Item(new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));

    public static final RegistryObject<Item> STICK_AND_GRASS = ITEMS.register("sedentism/stick_and_grass",
            StickAndGrass::new);



    public static final RegistryObject<Block> CHISELEDBLOCK = BLOCKS.register("sedentism/chiseled_stone", ChiseledStone::new);

    public static final RegistryObject<Item> CHISELDBLOCKITEM = BLOCKITEMS.register("sedentism/chiseled_stone",
            () -> new BlockItem(SedentismInit.CHISELEDBLOCK.get(),
                    new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));


}
