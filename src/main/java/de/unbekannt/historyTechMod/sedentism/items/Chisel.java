package de.unbekannt.historyTechMod.sedentism.items;

import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class Chisel extends Item {

    public static final String CHISEL1 = "message.chisel.1";
    public static final String CHISEL2 = "message.chisel.2";

    public Chisel() {
        super(new Properties().group(ModItemGroups.SEDENTISMGROUP).maxStackSize(1).defaultMaxDamage(30));
        
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new TranslationTextComponent(CHISEL1));
        tooltip.add(new TranslationTextComponent(CHISEL2));
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        BlockState clickedBlockState = context.getWorld().getBlockState(context.getPos());
        if(clickedBlockState.getBlock() == Blocks.STONE){
            context.getWorld().setBlockState(context.getPos(), SedentismInit.CHISELEDBLOCK.get().getDefaultState());
            context.getPlayer().playSound(SoundEvents.BLOCK_STONE_HIT, SoundCategory.BLOCKS, 1F, 1f);
            ItemStack chisel = context.getPlayer().getHeldItemMainhand();
            chisel.damageItem(1, context.getPlayer(), playerEntity -> playerEntity.sendBreakAnimation(EquipmentSlotType.MAINHAND));

            return ActionResultType.CONSUME;
        }
        return super.onItemUse(context);
    }

    @Override
    public boolean hasContainerItem(ItemStack stack) {
        return true;
    }

    @Override
    public ItemStack getContainerItem(ItemStack itemStack) {
        return itemStack;
    }
}
