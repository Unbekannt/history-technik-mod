package de.unbekannt.historyTechMod.sedentism.items;

import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import net.minecraft.item.FlintAndSteelItem;

public class StickAndGrass extends FlintAndSteelItem {

    public StickAndGrass() {
        super(new Properties().group(ModItemGroups.SEDENTISMGROUP).defaultMaxDamage(1));
    }


}
