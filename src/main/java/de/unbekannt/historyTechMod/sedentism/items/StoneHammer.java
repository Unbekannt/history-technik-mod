package de.unbekannt.historyTechMod.sedentism.items;

import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import net.minecraft.item.*;

public class StoneHammer extends PickaxeItem {

    public StoneHammer() {
        super(ItemTier.STONE, 1, -2.8f, new Properties().group(ModItemGroups.SEDENTISMGROUP));
    }

    @Override
    public boolean isEnchantable(ItemStack stack) {
        return false;
    }
}
