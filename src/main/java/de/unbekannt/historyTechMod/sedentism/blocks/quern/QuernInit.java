package de.unbekannt.historyTechMod.sedentism.blocks.quern;

import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import net.minecraft.block.Block;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.fml.RegistryObject;

import static de.unbekannt.historyTechMod.sedentism.SedentismInit.*;

public class QuernInit {
    //Quern
    public static final RegistryObject<Block> QUERN = BLOCKS.register("sedentism/quern", Quern::new);

    public static final RegistryObject<Item> QUERNBLOCKITEM = BLOCKITEMS.register("sedentism/quern",
            () -> new BlockItem(QUERN.get(),
                    new Item.Properties().group(ModItemGroups.SEDENTISMGROUP)));

    public static final RegistryObject<ContainerType<QuernContainer>> QUERNCONTAINER =
            CONTAINERTYPES.register("sedentism/quern", () -> IForgeContainerType.create(((windowId, inv, data) -> {
                BlockPos pos = data.readBlockPos();
                World world = inv.player.getEntityWorld();
                return new QuernContainer(windowId, world, pos, inv, inv.player);
            })));

    public static final RegistryObject<TileEntityType<QuernTileEntity>> QUERNENTITY =
            TILEENTITYS.register("sedentism/quern_entity",
                    () -> TileEntityType.Builder
                            .create(QuernTileEntity::new, QUERN.get())
                            .build(null));

}
