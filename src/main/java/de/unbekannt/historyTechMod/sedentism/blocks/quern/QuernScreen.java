package de.unbekannt.historyTechMod.sedentism.blocks.quern;

import com.mojang.blaze3d.matrix.MatrixStack;
import de.unbekannt.historyTechMod.HistoryTechMod;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class QuernScreen extends ContainerScreen<QuernContainer> {


    private ResourceLocation GUI = new ResourceLocation(HistoryTechMod.MODID, "textures/gui/sedentism/quern.png");

    public QuernScreen(QuernContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
        //xSize = 50;
        //ySize = 50;
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int x, int y) {
        this.minecraft.getTextureManager().bindTexture(GUI);
        this.blit(matrixStack, guiLeft, guiTop,0, 0, this.xSize, this.ySize);
    }

}
