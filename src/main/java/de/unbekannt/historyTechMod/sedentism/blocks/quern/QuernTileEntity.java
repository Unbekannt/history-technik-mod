package de.unbekannt.historyTechMod.sedentism.blocks.quern;

import de.unbekannt.historyTechMod.ageless.AgelessInit;
import de.unbekannt.historyTechMod.ageless.config.Config;
import de.unbekannt.historyTechMod.ageless.recipies.grinding.GrindingRecipe;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RecipeWrapper;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

public class QuernTileEntity extends TileEntity{


    private ItemStackHandler itemHandler = createHandler();

    private LazyOptional<IItemHandler> handler = LazyOptional.of(() -> itemHandler);



    private int counter;

    public QuernTileEntity() {
        super(QuernInit.QUERNENTITY.get());
    }


    @Override
    public void remove() {
        super.remove();
        handler.invalidate();
    }


    @Override
    public void read(BlockState state, CompoundNBT nbt) {
        itemHandler.deserializeNBT(nbt.getCompound("inv"));
        counter = nbt.getInt("counter");
        super.read(state, nbt);
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.put("inv", itemHandler.serializeNBT());

        compound.putInt("counter", counter);
        return super.write(compound);
    }

    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nonnull Direction side) {
        if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY){
            return handler.cast();
        }
        return super.getCapability(cap, side);
    }

    private ItemStackHandler createHandler() {
        return new ItemStackHandler(2) {

            @Override
            protected void onContentsChanged(int slot) {
                // To make sure the TE persists when the chunk is saved later we need to
                // mark it dirty every time the item handler changes
                markDirty();
            }

            @Override
            public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
                if(slot == 0 ) return stack.getItem() == SedentismInit.GRAIN.get();
                if (slot == 1) return stack.getItem() == SedentismInit.FLOUR.get();
                return false;
            }

            @Nonnull
            @Override
            public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                if (slot == 0 && stack.getItem() != SedentismInit.GRAIN.get()) {
                    return stack;
                }
                if(slot == 1 && stack.getItem() != SedentismInit.FLOUR.get()){
                    return stack;
                }
                return super.insertItem(slot, stack, simulate);
            }
        };
    }

    public boolean addTick() {
        if(this.world.isRemote && !itemHandler.getStackInSlot(0).isEmpty()){
            return true;
        }
        else if (this.world.isRemote && itemHandler.getStackInSlot(0).isEmpty()) {
            return false;
        }

        if(itemHandler.getStackInSlot(0).isEmpty()
                ||
                itemHandler.getStackInSlot(1).getMaxStackSize() == itemHandler.getStackInSlot(1).getCount()) return false;

        GrindingRecipe recipe = getRecipe(itemHandler.getStackInSlot(0));
        int timer = recipe.getTimer() / Config.QUERN_TICK_DIVIDER.get();

        if(counter == 0){
            startCrafting();
        }
        if(counter == timer){
            finishCrafting(recipe);
        }
        if (counter != timer){
            counter++;
        }

        return true;
    }
    
    private GrindingRecipe getRecipe(ItemStack stack){
        if (stack == null){
            return null;
        }
        Set<IRecipe<?>> recipes = findRecipeByType(AgelessInit.GRINDING_TYPE, this.world);
        for (IRecipe<?> iRecipe : recipes){
            if(!(iRecipe instanceof GrindingRecipe)) continue;
            GrindingRecipe grindingRecipe = (GrindingRecipe) iRecipe;
            if(grindingRecipe.matches(new RecipeWrapper(itemHandler), world)){
                return grindingRecipe;
            }
        }
        return null;
    }

    public static Set<IRecipe<?>> findRecipeByType(IRecipeType<?> recipeType) {
        ClientWorld world = Minecraft.getInstance().world;
        return world != null ?
                world.getRecipeManager().getRecipes().stream().filter(iRecipe -> iRecipe.getType() == recipeType).collect(Collectors.toSet())
                : Collections.emptySet();
    }

    public static Set<IRecipe<?>> findRecipeByType(IRecipeType<?> recipeType, World world) {
        return world != null ?
                world.getRecipeManager().getRecipes().stream().filter(iRecipe -> iRecipe.getType() == recipeType).collect(Collectors.toSet())
                : Collections.emptySet();
    }

    public static Set<ItemStack> getAllRecipeInputs(IRecipeType<?> type, World world){
        Set<ItemStack> inputs = new HashSet<>();
        Set<IRecipe<?>> recipes = findRecipeByType(type, world);
        for (IRecipe<?> recipe : recipes){
            NonNullList<Ingredient> ingredients = recipe.getIngredients();
            ingredients.forEach(ingredient -> inputs.addAll(Arrays.asList(ingredient.getMatchingStacks())));
        }
        return inputs;
    }

    private void startCrafting() {

    }

    private void finishCrafting(GrindingRecipe recipe) {
        ItemStack output = recipe.getRecipeOutput();
        itemHandler.insertItem(1, output, false);
        itemHandler.extractItem(0, 1, false);
        counter = 0;
        markDirty();
    }

}
