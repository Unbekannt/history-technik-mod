package de.unbekannt.historyTechMod.jeiPlugin;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IngredientStackListBuilder {

    private final List<List<ItemStack>> list;

    private IngredientStackListBuilder(){
        this.list = new ArrayList<>();
    }

    public static IngredientStackListBuilder make(Ingredient... ingredientStack){
        IngredientStackListBuilder builder =  new IngredientStackListBuilder();
        builder.add(ingredientStack);
        return builder;
    }

    private IngredientStackListBuilder add(Ingredient... ingredientStack) {
        for (Ingredient ingr : ingredientStack)
            this.list.add(Arrays.asList(ingr.getMatchingStacks()));

        return this;
    }

    public List<List<ItemStack>> build(){
        return list;
    }
}
