//package de.unbekannt.historyTechMod.jeiPlugin.ageless;
//
//import de.unbekannt.historyTechMod.HistoryTechMod;
//import de.unbekannt.historyTechMod.ageless.AgelessInit;
//import de.unbekannt.historyTechMod.ageless.recipies.grinding.GrindingRecipe;
//import de.unbekannt.historyTechMod.jeiPlugin.IngredientStackListBuilder;
//import de.unbekannt.historyTechMod.sedentism.SedentismInit;
//import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernInit;
//import mezz.jei.api.constants.VanillaTypes;
//import mezz.jei.api.gui.IRecipeLayout;
//import mezz.jei.api.gui.drawable.IDrawable;
//import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
//import mezz.jei.api.helpers.IGuiHelper;
//import mezz.jei.api.ingredients.IIngredients;
//import mezz.jei.api.recipe.category.IRecipeCategory;
//import net.minecraft.client.resources.I18n;
//import net.minecraft.item.ItemStack;
//import net.minecraft.util.ResourceLocation;
//import net.minecraft.util.text.TranslationTextComponent;
//
//import java.util.Arrays;
//
//public class JeiGrindingRecipeCategory implements IRecipeCategory<GrindingRecipe> {
//
//    private IGuiHelper guiHelper;
//    private IDrawable icon;
//
//    public JeiGrindingRecipeCategory(IGuiHelper guiHelper){
//        this.guiHelper = guiHelper;
//        this.icon = guiHelper.createDrawableIngredient(new ItemStack(QuernInit.QUERN.get()));
//    }
//
//    @Override
//    public ResourceLocation getUid() {
//        return new ResourceLocation(HistoryTechMod.MODID, "grinding");
//    }
//
//    @Override
//    public Class<? extends GrindingRecipe> getRecipeClass() {
//        return GrindingRecipe.class;
//    }
//
//    @Override
//    public String getTitle() {
//        return I18n.format("block.historytechnikmod.sedentism.quern");
//    }
//
//    @Override
//    public IDrawable getBackground() {
//        return null;
//    }
//
//    @Override
//    public IDrawable getIcon() {
//        return icon;
//    }
//
//    @Override
//    public void setIngredients(GrindingRecipe grindingRecipe, IIngredients iIngredients) {
//        iIngredients.setInputLists(VanillaTypes.ITEM, IngredientStackListBuilder.make(grindingRecipe.getInput()).build());
//        iIngredients.setOutput(VanillaTypes.ITEM, grindingRecipe.getRecipeOutput());
//    }
//
//    @Override
//    public void setRecipe(IRecipeLayout iRecipeLayout, GrindingRecipe grindingRecipe, IIngredients iIngredients) {
//        IGuiItemStackGroup itemStackGroup = iRecipeLayout.getItemStacks();
//        itemStackGroup.init(0, true, 21, 18);
//        itemStackGroup.set(0, Arrays.asList(grindingRecipe.getInput().getMatchingStacks()));
//
//        itemStackGroup.init(1, false, 76, 18);
//    }
//}
