//package de.unbekannt.historyTechMod.jeiPlugin;
//
//import de.unbekannt.historyTechMod.HistoryTechMod;
//import de.unbekannt.historyTechMod.ageless.AgelessInit;
//import de.unbekannt.historyTechMod.jeiPlugin.ageless.JeiGrindingRecipeCategory;
//import mezz.jei.api.IModPlugin;
//import mezz.jei.api.JeiPlugin;
//import mezz.jei.api.constants.VanillaRecipeCategoryUid;
//import mezz.jei.api.helpers.IGuiHelper;
//import mezz.jei.api.registration.*;
//import mezz.jei.api.runtime.IJeiRuntime;
//import net.minecraft.item.crafting.FurnaceRecipe;
//import net.minecraft.item.crafting.IRecipeType;
//import net.minecraft.item.crafting.RecipeManager;
//import net.minecraft.util.ResourceLocation;
//import net.minecraftforge.registries.ForgeRegistries;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//@JeiPlugin()
//public class HistoryTechnikModPlugin implements IModPlugin {
//
//    public static final ResourceLocation UUID = new ResourceLocation(HistoryTechMod.MODID);
//
//    @Override
//    public ResourceLocation getPluginUid() {
//        return UUID;
//    }
//
//    @Override
//    public void registerItemSubtypes(ISubtypeRegistration registration) {
//
//    }
//
//    @Override
//    public void registerIngredients(IModIngredientRegistration registration) {
//
//    }
//
//    @Override
//    public void registerCategories(IRecipeCategoryRegistration registration) {
//        IGuiHelper guiHelper = registration.getJeiHelpers().getGuiHelper();
//        registration.addRecipeCategories(new JeiGrindingRecipeCategory(guiHelper));
//    }
//
//    @Override
//    public void registerVanillaCategoryExtensions(IVanillaCategoryExtensionRegistration registration) {
//
//    }
//
//    @Override
//    public void registerRecipes(IRecipeRegistration registration) {
//
//    }
//
//    @Override
//    public void registerRecipeTransferHandlers(IRecipeTransferRegistration registration) {
//
//    }
//
//    @Override
//    public void registerRecipeCatalysts(IRecipeCatalystRegistration registration) {
//
//    }
//
//    @Override
//    public void registerGuiHandlers(IGuiHandlerRegistration registration) {
//
//    }
//
//    @Override
//    public void registerAdvanced(IAdvancedRegistration registration) {
//
//    }
//
//    @Override
//    public void onRuntimeAvailable(IJeiRuntime jeiRuntime) {
//    }
//}
