package de.unbekannt.historyTechMod.datagen.builder;

import com.google.gson.JsonObject;
import de.unbekannt.historyTechMod.ageless.AgelessInit;
import de.unbekannt.historyTechMod.ageless.recipies.grinding.GrindingRecipeSerializer;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.advancements.IRequirementsStrategy;
import net.minecraft.advancements.criterion.EntityPredicate;
import net.minecraft.advancements.criterion.RecipeUnlockedTrigger;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.function.Consumer;

public class GrinderRecipeBuilder {
    private final Item result;
    private final Ingredient ingredient;
    private final int grindingTime;
    private final Advancement.Builder advancementBuilder = Advancement.Builder.builder();
    private String group;
    private final GrindingRecipeSerializer recipeSerializer;
    private int count = 1;

    private GrinderRecipeBuilder(IItemProvider resultIn, Ingredient ingredientIn, int grindingTimeIn, GrindingRecipeSerializer serializer) {
        this.result = resultIn.asItem();
        this.ingredient = ingredientIn;
        this.grindingTime = grindingTimeIn;
        this.recipeSerializer = serializer;
    }

    public static GrinderRecipeBuilder grindingRecipe(Ingredient ingredientIn, IItemProvider resultIn, int grindingTimeIn) {
        return new GrinderRecipeBuilder(resultIn, ingredientIn, grindingTimeIn, AgelessInit.GRINDING.get());
    }

    public GrinderRecipeBuilder addCriterion(String name, ICriterionInstance criterionIn) {
        this.advancementBuilder.withCriterion(name, criterionIn);
        return this;
    }

    public GrinderRecipeBuilder setCount(int count){
        this.count = count;

        return this;
    }

    public void build(Consumer<IFinishedRecipe> consumerIn) {
        this.build(consumerIn, ForgeRegistries.ITEMS.getKey(this.result));
    }

    public void build(Consumer<IFinishedRecipe> consumerIn, String save) {
        ResourceLocation resourcelocation = ForgeRegistries.ITEMS.getKey(this.result);
        ResourceLocation resourcelocation1 = new ResourceLocation(save);
        if (resourcelocation1.equals(resourcelocation)) {
            throw new IllegalStateException("Recipe " + resourcelocation1 + " should remove its 'save' argument");
        } else {
            this.build(consumerIn, resourcelocation1);
        }
    }

    public void build(Consumer<IFinishedRecipe> consumerIn, ResourceLocation id) {
        this.validate(id);
        this.advancementBuilder.withParentId(
                new ResourceLocation("recipes/root"))
                .withCriterion("has_the_recipe",
                        new RecipeUnlockedTrigger.Instance(EntityPredicate.AndPredicate.ANY_AND, id))
                .withRewards(AdvancementRewards.Builder.recipe(id))
                .withRequirementsStrategy(IRequirementsStrategy.OR);
        consumerIn.accept(
                new Result(
                        id,
                        this.group == null ? "" : this.group,
                        this.ingredient,
                        this.result,
                        this.count,
                        this.grindingTime,
                        this.advancementBuilder,
                        new ResourceLocation(id.getNamespace(),
                                "recipes/" +
                                        this.result.getGroup().getPath() +
                                        "/" + id.getPath()),
                        this.recipeSerializer));
    }

    /**
     * Makes sure that this obtainable.
     */
    private void validate(ResourceLocation id) {
        if (this.advancementBuilder.getCriteria().isEmpty()) {
            throw new IllegalStateException("No way of obtaining recipe " + id);
        }
    }

    public static class Result implements IFinishedRecipe {
        private final ResourceLocation id;
        private final String group;
        private final Ingredient ingredient;
        private final Item result;
        private final int count;
        private final int grindingTime;
        private final Advancement.Builder advancementBuilder;
        private final ResourceLocation advancementId;
        private final GrindingRecipeSerializer serializer;

        public Result(ResourceLocation idIn, String groupIn, Ingredient ingredientIn, Item resultIn, int count,  int grindingTimeIn, Advancement.Builder advancementBuilderIn, ResourceLocation advancementIdIn, GrindingRecipeSerializer serializerIn) {
            this.id = idIn;
            this.group = groupIn;
            this.ingredient = ingredientIn;
            this.result = resultIn;
            this.count = count;
            this.grindingTime = grindingTimeIn;
            this.advancementBuilder = advancementBuilderIn;
            this.advancementId = advancementIdIn;
            this.serializer = serializerIn;
        }

        public void serialize(JsonObject json) {
            if (!this.group.isEmpty()) {
                json.addProperty("group", this.group);
            }

            json.add("ingredient", this.ingredient.serialize());
            JsonObject result = new JsonObject();
            result.addProperty("item", ForgeRegistries.ITEMS.getKey(this.result).toString());
            result.addProperty("count", count);
            json.add("result", result);
            json.addProperty("grindTime", this.grindingTime);
        }

        public IRecipeSerializer<?> getSerializer() {
            return this.serializer;
        }

        /**
         * Gets the ID for the recipe.
         */
        public ResourceLocation getID() {
            return this.id;
        }

        /**
         * Gets the JSON for the advancement that unlocks this recipe. Null if there is no advancement.
         */
        @Nullable
        public JsonObject getAdvancementJson() {
            return this.advancementBuilder.serialize();
        }

        /**
         * Gets the ID for the advancement associated with this recipe. Should not be null if {@link #getAdvancementJson}
         * is non-null.
         */
        @Nullable
        public ResourceLocation getAdvancementID() {
            return this.advancementId;
        }
    }

}
