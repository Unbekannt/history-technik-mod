package de.unbekannt.historyTechMod.datagen.items;


import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace.EarlyAlloyFurnaceInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceInit;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.ItemModelProvider;

public class MetalAgeItems {

    private ItemModelProvider handler;

    public static final String items = "item/metalage/";
    public static final String blocks = "block/metalage/";

    public MetalAgeItems(ItemModelProvider handler){
        this.handler = handler;
        init();
    }

    public void init(){
        handler.singleTexture("item/"+ MetalAgeInit.COPPER_PICKAXE.get().getRegistryName().getPath(),
                new ResourceLocation("item/handheld"),
                "layer0",
                new ResourceLocation(HistoryTechMod.MODID, items+"copper_pickaxe"));

//        handler.singleTexture("item/"+MetalAgeInit.BRONZE_TONG.get().getRegistryName().getPath(),
//                new ResourceLocation("item/handheld"),
//                "layer0",
//                new ResourceLocation(HistoryTechMod.MODID, items+"bronze_tong"));

        handler.withExistingParent("item/"+ MetalAgeInit.COPPER_ORE_ITEM.get().getRegistryName().getPath(),
                new ResourceLocation(HistoryTechMod.MODID, blocks+"copper_ore"));

        handler.withExistingParent("item/"+ MetalAgeInit.TIN_ORE_ITEM.get().getRegistryName().getPath(),
                new ResourceLocation(HistoryTechMod.MODID, blocks+"tin_ore"));

        handler.withExistingParent("item/"+ EarlyFurnaceInit.earlyFurnace.get().getRegistryName().getPath(),
                new ResourceLocation(HistoryTechMod.MODID, blocks+"early_furnace"));

        handler.withExistingParent("item/"+ EarlyAlloyFurnaceInit.earlyAlloyFurnace.get().getRegistryName().getPath(),
                new ResourceLocation(HistoryTechMod.MODID, blocks+"early_alloy_furnace/early_alloy_furnace"));
    }
}
