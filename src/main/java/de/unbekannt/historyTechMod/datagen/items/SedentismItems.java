package de.unbekannt.historyTechMod.datagen.items;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernInit;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.ItemModelProvider;

public class SedentismItems {

    private final ItemModelProvider handler;

    public SedentismItems(ItemModelProvider items){
        this.handler = items;
        init();
    }

    private void init() {
        handler.singleTexture("item/"+ SedentismInit.STONE_HAMMER.get().getRegistryName().getPath(),
                new ResourceLocation("item/handheld"),
                "layer0",
                new ResourceLocation(HistoryTechMod.MODID, "item/sedentism/stone_hammer"));

        handler.singleTexture("item/"+SedentismInit.CHISEL.get().getRegistryName().getPath(),
                new ResourceLocation("item/handheld"),
                "layer0",
                new ResourceLocation(HistoryTechMod.MODID, "item/sedentism/chisel"));
        handler.withExistingParent("item/"+ QuernInit.QUERNBLOCKITEM.get().getRegistryName().getPath(),
                new ResourceLocation(HistoryTechMod.MODID, "block/sedentism/quern"));
        handler.withExistingParent("item/"+SedentismInit.CHISELDBLOCKITEM.get().getRegistryName().getPath(),
                new ResourceLocation("minecraft", "block/stone") );
    }
}
