package de.unbekannt.historyTechMod.datagen.items;

import de.unbekannt.historyTechMod.HistoryTechMod;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;

public class Items extends ItemModelProvider {


    public Items(DataGenerator generator,  ExistingFileHelper existingFileHelper) {
        super(generator, HistoryTechMod.MODID, existingFileHelper);
    }

    @Override
    protected void registerModels() {
        new SedentismItems(this);
        new MetalAgeItems(this);
    }

    @Override
    public String getName() {
        return "History Technik Mod Items";
    }
}
