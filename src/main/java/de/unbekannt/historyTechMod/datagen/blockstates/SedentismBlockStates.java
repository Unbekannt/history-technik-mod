package de.unbekannt.historyTechMod.datagen.blockstates;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernInit;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockModelBuilder;

public class SedentismBlockStates {

    public static void init(BlockStates bs){
        registerChiseledBlock(bs);
        registerQuern(bs);
    }

    private static void registerChiseledBlock(BlockStates bs) {
        BlockModelBuilder modleChiseledBlock =
                bs.models().cubeAll("block/sedentism/chiseled_stone",
                        new ResourceLocation("minecraft", "block/stone"));
        bs.simpleBlock(SedentismInit.CHISELEDBLOCK.get(), modleChiseledBlock);
    }

    private static void registerQuern(BlockStates bs) {
        ResourceLocation txt = new ResourceLocation("minecraft", "block/stone");
        ResourceLocation up = new ResourceLocation(HistoryTechMod.MODID, "block/sedentism/quern_top");
        BlockModelBuilder modelQuern = bs.models().cubeBottomTop("block/sedentism/quern", txt, txt, up);
        bs.orientedBlock(QuernInit.QUERN.get(), $ -> modelQuern);
    }
}
