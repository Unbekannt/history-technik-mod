package de.unbekannt.historyTechMod.datagen.blockstates;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceInit;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockModelBuilder;

public class MetalAgeBlockStates {

    public static void init(BlockStates bs){
        registerCopperOre(bs);
        registerTinOre(bs);
        registerEarlyFurnace(bs);
    }

    private static void registerEarlyFurnace(BlockStates bs) {
        ResourceLocation side = new ResourceLocation("minecraft", "block/furnace_side");
        ResourceLocation front = new ResourceLocation("minecraft", "block/furnace_front");
        ResourceLocation frontLit = new ResourceLocation("minecraft", "block/furnace_front_on");
        ResourceLocation top_bottom = new ResourceLocation("minecraft", "block/furnace_top");

        BlockModelBuilder modleFurnaceUnLit =
                bs.models().orientable("block/metalage/early_furnace", side, front, top_bottom);
        BlockModelBuilder modleFurnaceLit =
                bs.models().orientable("block/metalage/early_furnace_lit",side , frontLit, top_bottom);
        bs.orientedBlock(EarlyFurnaceInit.earlyFurnace.get(), blockState -> {
            if(blockState.get(BlockStateProperties.LIT)){
                return modleFurnaceLit;
            }
            else {
                return modleFurnaceUnLit;
            }
        });
    }

    private static void registerTinOre(BlockStates bs) {
        //TODO: CHANGE TEXTURE
        BlockModelBuilder modleCopperOre =
                bs.models().cubeAll("block/metalage/tin_ore",
                        new ResourceLocation(HistoryTechMod.MODID, "block/metalage/tin_ore"));
        bs.simpleBlock(MetalAgeInit.TIN_ORE.get(), modleCopperOre);
    }

    private static void registerCopperOre(BlockStates bs) {
        BlockModelBuilder modleCopperOre =
                bs.models().cubeAll("block/metalage/copper_ore",
                        new ResourceLocation(HistoryTechMod.MODID, "block/metalage/copper_ore"));
        bs.simpleBlock(MetalAgeInit.COPPER_ORE.get(), modleCopperOre);
    }


}
