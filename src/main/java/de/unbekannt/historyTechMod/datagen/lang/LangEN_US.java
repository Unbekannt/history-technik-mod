package de.unbekannt.historyTechMod.datagen.lang;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.ageless.ModItemGroups;
import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace.EarlyAlloyFurnaceInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace.EarlyAlloyFurnaceTileEntity;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceTileEntity;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.Quern;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernInit;
import de.unbekannt.historyTechMod.sedentism.items.Chisel;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.LanguageProvider;

public class LangEN_US extends LanguageProvider {

    public LangEN_US(DataGenerator gen) {
        super(gen, HistoryTechMod.MODID, "en_us");
    }

    @Override
    protected void addTranslations() {
        //ModGroups
        add(ModItemGroups.SEDENTISMGROUP.getPath(), "Sedentism");
        add(ModItemGroups.METALAGEGROUP.getPath(), "Metalage");

        //Sedentism
        add(SedentismInit.BRANCH.get(), "Branch");
        add(SedentismInit.CHISEL.get(), "CHISEL");
        add(SedentismInit.FLAIL.get(), "Flail");
        add(SedentismInit.FLOUR.get(), "Flour");
        add(SedentismInit.GRAIN.get(), "Grain");
        add(SedentismInit.HANDSTONE.get(), "Hand Stone");
        add(SedentismInit.STONE_HAMMER.get(), "Stone Hammer");
        add(SedentismInit.STONECENTER.get(), "Stone Center");
        add(SedentismInit.STONEEDGE.get(), "Stone Edge");
        add(SedentismInit.CHISELEDBLOCK.get(), "Chiseled Stone");
        add(SedentismInit.DRY_GRASS.get(), "Dry Grass");
        add(SedentismInit.STICK_AND_GRASS.get(), "Stick and Grass");
        add(QuernInit.QUERN.get(), "Mill Stone");
        add(Quern.SCREENNAME, "Mill Stone");
        add(Chisel.CHISEL1, "Right click a Stone and then destroy ");
        add(Chisel.CHISEL2, "the Stone with a Stone Hammer to get Stone");

        //Metalage
        add(MetalAgeInit.COPPER_INGOT.get(), "Copper Ingot");
        add(MetalAgeInit.COPPER_ORE.get(), "Copper Ore");
        add(MetalAgeInit.COPPER_PICKAXE.get(), "Copper Pickaxe");
        add(MetalAgeInit.TIN_INGOT.get(), "Tin Ingot");
        add(MetalAgeInit.TIN_ORE.get(), "Tin Ore");
        add(MetalAgeInit.BRONZE_INGOT.get(), "Bronze Ingot");
        add(EarlyAlloyFurnaceInit.earlyAlloyFurnace.get(),  "Alloy Furnace");
        add(EarlyAlloyFurnaceTileEntity.SCREEN_NAME, "Alloy Furnace");
        add(EarlyFurnaceInit.earlyFurnace.get(), "Early Furnace");
        add(EarlyFurnaceTileEntity.SCREENNAME, "Early Furnace");
        add(MetalAgeInit.BRONZE_TONG.get(), "Blacksmith's Tongs");
    }
}
