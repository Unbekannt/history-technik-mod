package de.unbekannt.historyTechMod.datagen.datagenerators;

import de.unbekannt.historyTechMod.datagen.lang.LangEN_US;
import de.unbekannt.historyTechMod.datagen.loottables.GlobalLootModifier;
import de.unbekannt.historyTechMod.datagen.tags.BlockTags;
import de.unbekannt.historyTechMod.datagen.tags.ItemTags;
import de.unbekannt.historyTechMod.datagen.blockstates.BlockStates;
import de.unbekannt.historyTechMod.datagen.items.Items;
import de.unbekannt.historyTechMod.datagen.loottables.LootTables;
import de.unbekannt.historyTechMod.datagen.recipies.Recipes;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGenerators {

    @SubscribeEvent
    public static void gatherData(GatherDataEvent event){
        DataGenerator generator = event.getGenerator();
        if(event.includeServer()){
            generator.addProvider(new Recipes(generator));
            generator.addProvider(new LootTables(generator));
            generator.addProvider(new ItemTags(generator, event.getExistingFileHelper()));
            generator.addProvider(new BlockTags(generator, event.getExistingFileHelper()));
            generator.addProvider(new LangEN_US(generator));
            generator.addProvider(new GlobalLootModifier(generator));
        }
        if(event.includeClient()){
            generator.addProvider(new BlockStates(generator, event.getExistingFileHelper()));
            generator.addProvider(new Items(generator, event.getExistingFileHelper()));
        }
    }
}
