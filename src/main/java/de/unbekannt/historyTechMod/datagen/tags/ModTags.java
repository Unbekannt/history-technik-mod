package de.unbekannt.historyTechMod.datagen.tags;

import de.unbekannt.historyTechMod.HistoryTechMod;

public enum ModTags {
    COPPER_ORE("forge:ore/copper"),
    COPPER_INGOT("forge:ingots/copper"),
    TIN_ORE("forge:ore/tin"),
    TIN_INGOT("forge:ingots/tin"),
    FLOUR("forge:dusts/flour"),
    STRIPPEDLOGS(HistoryTechMod.MODID+":stripped_logs"),
    STRIPPEDWOOD(HistoryTechMod.MODID+":stripped_wood"),
    STRIPPEDWOODSANDLOGS(HistoryTechMod.MODID+":stripped_logs_woods");

    private String key;

    ModTags(String key) {
        this.key = key;
    }

    public String getKey(){
        return key;
    }
}
