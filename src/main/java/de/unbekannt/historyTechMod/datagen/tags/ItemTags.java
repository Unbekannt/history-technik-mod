package de.unbekannt.historyTechMod.datagen.tags;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.ItemTagsProvider;
import net.minecraft.item.Item;
import net.minecraft.tags.ITag;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.stream.Collectors;

public class ItemTags extends ItemTagsProvider {

    public static final ITag.INamedTag<Item> strippedLogs =
            net.minecraft.tags.ItemTags.makeWrapperTag(ModTags.STRIPPEDLOGS.getKey());
    public static final ITag.INamedTag<Item> strippedWoods =
            net.minecraft.tags.ItemTags.makeWrapperTag(ModTags.STRIPPEDWOOD.getKey());

    public static final ITag.INamedTag<Item> strippedWoodsAndLogs =
            net.minecraft.tags.ItemTags.makeWrapperTag(ModTags.STRIPPEDWOODSANDLOGS.getKey());

    public ItemTags(DataGenerator dataGenerator, @Nullable ExistingFileHelper existingFileHelper) {
        super(dataGenerator, new BlockTagsProvider(dataGenerator, HistoryTechMod.MODID, existingFileHelper), HistoryTechMod.MODID, existingFileHelper);
    }

    @Override
    protected void registerTags() {
        registerStrippedLogs();
        registerStrippedWood();
        registerStrippedLogsAndWood();
        registerToForgeDusts();
        registerToForgeOre();
        registerToForgeIngots();
    }

    private void registerToForgeIngots() {
        getOrCreateBuilder(net.minecraft.tags.ItemTags.makeWrapperTag(ModTags.COPPER_INGOT.getKey()))
                .add(MetalAgeInit.COPPER_INGOT.get());
        getOrCreateBuilder(net.minecraft.tags.ItemTags.makeWrapperTag(ModTags.TIN_INGOT.getKey()))
                .add(MetalAgeInit.TIN_INGOT.get());
    }

    private void registerToForgeOre() {

        getOrCreateBuilder(net.minecraft.tags.ItemTags.makeWrapperTag(ModTags.COPPER_ORE.getKey()))
                .add(MetalAgeInit.COPPER_ORE_ITEM.get());

        getOrCreateBuilder(net.minecraft.tags.ItemTags.makeWrapperTag(ModTags.TIN_ORE.getKey()))
                .add(MetalAgeInit.TIN_ORE_ITEM.get());

    }

    private void registerToForgeDusts() {
        getOrCreateBuilder(net.minecraft.tags.ItemTags.makeWrapperTag(ModTags.FLOUR.getKey()))
                .add(SedentismInit.FLOUR.get());
    }

    private void registerStrippedLogsAndWood() {
        getOrCreateBuilder(strippedWoodsAndLogs)
                .addTag(strippedLogs)
                .addTag(strippedWoods);
    }

    private void registerStrippedWood() {
        Builder<Item> woods = getOrCreateBuilder(strippedWoods);

        List<Item> items = ForgeRegistries.ITEMS.getValues().stream().filter(item -> {
            final String name = item.getRegistryName().toString();
            return name.contains("wood") && name.contains("stripped");
        }).collect(Collectors.toList());

        for (Item item : items){
            woods.add(item);
        }
    }

    private void registerStrippedLogs() {

        Builder<Item> logs = getOrCreateBuilder(strippedLogs);

        List<Item> items = ForgeRegistries.ITEMS.getValues().stream().filter(item -> {
            final String name = item.getRegistryName().toString();
            return name.contains("log") && name.contains("stripped");
        }).collect(Collectors.toList());

        for (Item item : items){
            logs.add(item);
        }
    }
}
