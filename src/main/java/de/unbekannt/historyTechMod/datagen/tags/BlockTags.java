package de.unbekannt.historyTechMod.datagen.tags;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import org.jetbrains.annotations.Nullable;

public class BlockTags extends BlockTagsProvider {

    public BlockTags(DataGenerator generatorIn, @Nullable ExistingFileHelper existingFileHelper) {
        super(generatorIn, HistoryTechMod.MODID, existingFileHelper);
    }

    @Override
    protected void registerTags() {
        registerToForgeOres();
    }

    private void registerToForgeOres() {
        String copper = ModTags.COPPER_ORE.getKey();
        String tin = ModTags.TIN_ORE.getKey();

        getOrCreateBuilder(net.minecraft.tags.BlockTags.makeWrapperTag(copper))
                .add(MetalAgeInit.COPPER_ORE.get());

        getOrCreateBuilder(net.minecraft.tags.BlockTags.makeWrapperTag(tin))
                .add(MetalAgeInit.TIN_ORE.get());
    }
}
