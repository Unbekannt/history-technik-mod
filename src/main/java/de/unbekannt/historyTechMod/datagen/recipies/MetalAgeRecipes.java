package de.unbekannt.historyTechMod.datagen.recipies;

import de.unbekannt.historyTechMod.ageless.recipies.ConfigCondition;
import de.unbekannt.historyTechMod.datagen.builder.AlloyFurnaceRecipeBuilder;
import de.unbekannt.historyTechMod.datagen.builder.CustomFurnaceBuilder;
import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.data.CookingRecipeBuilder;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.data.ShapedRecipeBuilder;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.crafting.ConditionalRecipe;

import java.util.function.Consumer;

public class MetalAgeRecipes {
    private Consumer<IFinishedRecipe> consumer;

    public MetalAgeRecipes(Consumer<IFinishedRecipe> consumer){
        this.consumer = consumer;
        init();
    }

    private ICriterionInstance hasItem(Tags.IOptionalNamedTag<Item> item) {
        return Recipes.externalHasItem(item);
    }

    private ICriterionInstance hasItem(Item item) {
        return Recipes.externalHasItem(item);
    }

    private void init(){
        registerEarlyCopper();
        registerEarlyTin();
        registerFurnace();
        registerBronze();
        registerTong();
    }

    private void registerTong() {
        ShapedRecipeBuilder.shapedRecipe(MetalAgeInit.BRONZE_TONG.get())
                .patternLine("b b")
                .patternLine(" b ")
                .patternLine("b b")
                .key('b', MetalAgeInit.BRONZE_INGOT.get())
                .addCriterion("has_bronze", hasItem(MetalAgeInit.BRONZE_INGOT.get()))
                .build(consumer);
    }

    private void registerBronze() {
        AlloyFurnaceRecipeBuilder.alloyRecipe(MetalAgeInit.BRONZE_INGOT.get(),  3, 200)
        .addCriterion("has_allowFurnace",hasItem(MetalAgeInit.TIN_INGOT.get()))
        .addMaterial(Ingredient.fromItems(MetalAgeInit.COPPER_INGOT.get()), 3)
        .addMaterial(Ingredient.fromItems(MetalAgeInit.TIN_INGOT.get()))
        .build(consumer);

    }

    private void registerFurnace() {
        ConditionalRecipe.builder()
                .addCondition(new ConfigCondition(true, "alternative_Recipe"))
                .addRecipe(
                        ShapedRecipeBuilder.shapedRecipe(Items.FURNACE)
                                .patternLine("ccc")
                                .patternLine("ctc")
                                .patternLine("csc")
                                .key('c', Tags.Items.COBBLESTONE)
                                .key('s', SedentismInit.STONECENTER.get())
                                .key('t', MetalAgeInit.BRONZE_TONG.get())
                                .addCriterion("has_center", hasItem(SedentismInit.STONECENTER.get()))
                                ::build
                )
                .build(consumer, new ResourceLocation("minecraft", "furnace"));
    }

    private void registerEarlyTin() {
        CustomFurnaceBuilder
                .create(Ingredient.fromItems(
                        MetalAgeInit.TIN_ORE_ITEM.get()),
                        MetalAgeInit.TIN_INGOT.get(),
                        200)
                .addCriterion("has_tin_ore", Recipes.externalHasItem(MetalAgeInit.TIN_ORE_ITEM.get()))
                .build(consumer, "tin_ingot_early_furnace");

        CookingRecipeBuilder
                .smeltingRecipe(
                        Ingredient.fromItems(MetalAgeInit.TIN_ORE_ITEM.get()),
                        MetalAgeInit.TIN_INGOT.get(),
                        0,
                        200)
                .addCriterion("has_Furnace", Recipes.externalHasItem(Items.FURNACE))
                .build(consumer);
    }

    private void registerEarlyCopper() {
        CustomFurnaceBuilder
                .create(Ingredient.fromItems(
                        MetalAgeInit.COPPER_ORE_ITEM.get()),
                        MetalAgeInit.COPPER_INGOT.get(),
                        200)
                .addCriterion("has_copper_ore", Recipes.externalHasItem(MetalAgeInit.COPPER_ORE_ITEM.get()))
                .build(consumer, "copper_ingot_early_furnace");

        CookingRecipeBuilder
                .smeltingRecipe(
                        Ingredient.fromItems(MetalAgeInit.COPPER_ORE_ITEM.get()),
                        MetalAgeInit.COPPER_INGOT.get(),
                        0,
                        200)
                .addCriterion("has_Furnace", Recipes.externalHasItem(Items.FURNACE))
                .build(consumer);
    }
}
