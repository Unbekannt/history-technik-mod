package de.unbekannt.historyTechMod.datagen.recipies;

import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.data.*;
import net.minecraft.item.Item;
import net.minecraft.tags.ITag;

import java.util.function.Consumer;

public class Recipes extends RecipeProvider {

    public Recipes(DataGenerator generatorIn) {
        super(generatorIn);
    }

    @Override
    protected void registerRecipes(Consumer<IFinishedRecipe> consumer) {
        new SedentismRecipes(consumer);
        new MetalAgeRecipes(consumer);
    }

    //expose the protected internal methods so i can use tem in external classes
    public static ICriterionInstance externalHasItem(Item item) {
        return hasItem(item);
    }

    public static ICriterionInstance externalHasItem(ITag<Item> tagItem) {
        return hasItem(tagItem);
    }
}
