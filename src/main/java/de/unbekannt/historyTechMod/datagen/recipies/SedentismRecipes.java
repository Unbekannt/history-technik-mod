package de.unbekannt.historyTechMod.datagen.recipies;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.ageless.recipies.ConfigCondition;
import de.unbekannt.historyTechMod.datagen.builder.GrinderRecipeBuilder;
import de.unbekannt.historyTechMod.datagen.tags.ItemTags;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceInit;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernInit;
import net.minecraft.advancements.ICriterionInstance;
import net.minecraft.data.CookingRecipeBuilder;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.data.ShapedRecipeBuilder;
import net.minecraft.data.ShapelessRecipeBuilder;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.crafting.ConditionalRecipe;

import java.util.function.Consumer;

public class SedentismRecipes {

    private Consumer<IFinishedRecipe> consumer;

    public SedentismRecipes(Consumer<IFinishedRecipe> consumer){
        this.consumer = consumer;
        init();
    }

    private void init() {
        registerBranch();
        registerChisel();
        registerFlail();
        registerhand_stone();
        registerQuern();
        registerStoneCenter();
        registerStoneEdge();
        registerStoneHammer();
        registerFlour();
        registerBaking();
        registerEarlyFurnace();
        registerStickAndGrass();
    }

    private void registerStickAndGrass() {
        ShapelessRecipeBuilder.shapelessRecipe(SedentismInit.STICK_AND_GRASS.get())
                .addIngredient(Items.STICK)
                .addIngredient(SedentismInit.DRY_GRASS.get())
                .addCriterion("has_dry_gras", hasItem(SedentismInit.DRY_GRASS.get()))
                .build(consumer);
    }

    private void registerEarlyFurnace() {
        ConditionalRecipe.builder()
                .addCondition(new ConfigCondition(true, "alternative_Recipe"))
                .addRecipe(
                        ShapedRecipeBuilder.shapedRecipe(EarlyFurnaceInit.earlyFurnace.get())
                                .patternLine("ccc")
                                .patternLine("c c")
                                .patternLine("csc")
                                .key('c', Items.COBBLESTONE)
                                .key('s', SedentismInit.STONECENTER.get())
                                .addCriterion("has_center", hasItem(SedentismInit.STONECENTER.get()))
                                ::build
                )
                .addCondition(new ConfigCondition(false, "alternative_Recipe"))
                .addRecipe(
                        ShapedRecipeBuilder.shapedRecipe(Items.FURNACE)
                                .patternLine("ccc")
                                .patternLine("c c")
                                .patternLine("ccc")
                                .key('c', Items.COBBLESTONE)
                                .addCriterion("has_cobble", hasItem(Tags.Items.COBBLESTONE))
                        ::build
                )
                .build(consumer, new ResourceLocation(HistoryTechMod.MODID, "early_furnace"));
    }

    private void registerBaking() {
        CookingRecipeBuilder
                .smeltingRecipe(
                        Ingredient.fromTag(net.minecraft.tags.ItemTags.makeWrapperTag("forge:dusts/flour")),
                        Items.BREAD, 0.1f, 200)
                .addCriterion("has_flour", hasItem(SedentismInit.FLOUR.get()))
                .build(consumer, "cooking_bread");
    }

    private ICriterionInstance hasItem(Item item) {
        return Recipes.externalHasItem(item);
    }

    private ICriterionInstance hasItem(ITag<Item> tagItem){
        return Recipes.externalHasItem(tagItem);
    }

    private void registerFlour() {
        GrinderRecipeBuilder.grindingRecipe(Ingredient.fromItems(SedentismInit.GRAIN.get()), SedentismInit.FLOUR.get(), 40)
                .setCount(2)
                .addCriterion("has_grain", hasItem(SedentismInit.GRAIN.get()))
                .build(consumer);
    }

    private void registerStoneHammer() {
        ShapedRecipeBuilder.shapedRecipe(SedentismInit.STONE_HAMMER.get())
                .patternLine("c")
                .patternLine("s")
                .patternLine("d")
                .key('c', Tags.Items.COBBLESTONE)
                .key('s', Tags.Items.STRING)
                .key('d', Tags.Items.RODS_WOODEN)
                .setGroup("stoneHammer")
                .addCriterion("stick", hasItem(Tags.Items.RODS_WOODEN))
                .build(consumer);
    }

    private void registerStoneEdge() {
        ShapedRecipeBuilder.shapedRecipe(SedentismInit.STONEEDGE.get())
                .patternLine("c")
                .patternLine("c")
                .key('c', Tags.Items.STONE)
                .setGroup("stoneEdge")
                .addCriterion("stone", hasItem(Tags.Items.STONE))
                .build(consumer);

    }

    private void registerStoneCenter() {
        ShapedRecipeBuilder.shapedRecipe(SedentismInit.STONECENTER.get())
                .patternLine("scs")
                .patternLine("sss")
                .key('s', Tags.Items.STONE)
                .key('c', SedentismInit.CHISEL.get())
                .setGroup("stoneCenter")
                .addCriterion("chisel", hasItem(SedentismInit.CHISEL.get()))
                .build(consumer);
    }

    private void registerQuern() {
        ShapedRecipeBuilder.shapedRecipe(QuernInit.QUERNBLOCKITEM.get())
                .patternLine("xax")
                .patternLine("sss")
                .key('x', SedentismInit.STONEEDGE.get())
                .key('a', SedentismInit.STONECENTER.get())
                .key('s', Tags.Items.STONE)
                .setGroup("StoneEdge")
                .addCriterion("stonecenter", hasItem(SedentismInit.STONECENTER.get()))
                .build(consumer);
    }

    private void registerhand_stone() {
        ShapedRecipeBuilder.shapedRecipe(SedentismInit.HANDSTONE.get())
                .patternLine(" c ")
                .patternLine("c c")
                .patternLine(" c ")
                .key('c', Tags.Items.STONE)
                .setGroup("HandStone")
                .addCriterion("stone", hasItem(Tags.Items.STONE))
                .build(consumer);
    }

    private void registerFlail() {
        ShapedRecipeBuilder.shapedRecipe(SedentismInit.FLAIL.get())
                .patternLine(" bs")
                .patternLine("b r")
                .key('b', SedentismInit.BRANCH.get())
                .key('s', Items.STRING)
                .key('r', Items.STICK)
                .setGroup("flail")
                .addCriterion("branch", hasItem(SedentismInit.BRANCH.get()))
                .build(consumer);
    }

    private void registerChisel() {
        ShapedRecipeBuilder.shapedRecipe(SedentismInit.CHISEL.get())
                .patternLine("c c")
                .patternLine("c c")
                .patternLine(" c ")
                .key('c', Tags.Items.COBBLESTONE)
                .setGroup("Chisel")
                .addCriterion("cobblestone", hasItem(Tags.Items.COBBLESTONE))
                .build(consumer);
    }

    private void registerBranch() {
        ShapedRecipeBuilder.shapedRecipe(SedentismInit.BRANCH.get())
                .patternLine("b")
                .patternLine("b")
                .patternLine("b")
                .key('b', ItemTags.strippedWoodsAndLogs)
                .setGroup("Branch")
                .addCriterion("strippedLog", hasItem(ItemTags.strippedWoodsAndLogs))
                .build(consumer);
    }

}
