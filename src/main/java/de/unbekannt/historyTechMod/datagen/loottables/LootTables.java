package de.unbekannt.historyTechMod.datagen.loottables;

import de.unbekannt.historyTechMod.datagen.util.BaseLootTableProvider;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.loot.LootTable;

import java.util.Map;

public class LootTables extends BaseLootTableProvider {


    public LootTables(DataGenerator dataGeneratorIn) {
        super(dataGeneratorIn);
    }

    @Override
    protected void addTables() {
        SedentismLootTables.init(this);
        MetalAgeLootTables.init(this);
    }

    public Map<Block, LootTable.Builder> getLootTables(){
        return lootTables;
    }
}
