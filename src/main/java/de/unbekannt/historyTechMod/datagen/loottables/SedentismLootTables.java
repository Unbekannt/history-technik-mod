package de.unbekannt.historyTechMod.datagen.loottables;

import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import de.unbekannt.historyTechMod.sedentism.blocks.quern.QuernInit;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.loot.LootTable;

import java.util.Map;

public class SedentismLootTables {

    public static void init(LootTables loot){
        Map<Block, LootTable.Builder> loottable = loot.getLootTables();

        loottable.put(SedentismInit.CHISELEDBLOCK.get(),
                loot.createStandardTable("chiseled_stone", Blocks.COBBLESTONE));

        loottable.put(QuernInit.QUERN.get(),
                loot.createStandardTable("sedentism/quern",
                        QuernInit.QUERN.get()));

    }
}
