package de.unbekannt.historyTechMod.datagen.loottables;

import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyAlloyFurnace.EarlyAlloyFurnaceInit;
import de.unbekannt.historyTechMod.metalage.blocks.earlyFurnace.EarlyFurnaceInit;
import net.minecraft.block.Block;
import net.minecraft.loot.LootTable;

import java.util.Map;

public class MetalAgeLootTables {

    public static void init(LootTables loot){
        Map<Block, LootTable.Builder> loottable = loot.getLootTables();

        loottable.put(MetalAgeInit.COPPER_ORE.get(),
                loot.createStandardTable(MetalAgeInit.NAMEPREFIX+"copper_ore", MetalAgeInit.COPPER_ORE.get()));

        loottable.put(MetalAgeInit.TIN_ORE.get(),
                loot.createStandardTable(MetalAgeInit.NAMEPREFIX+"tin_ore", MetalAgeInit.TIN_ORE.get()));

        loottable.put(EarlyFurnaceInit.earlyFurnace.get(),
                loot.createStandardTable(MetalAgeInit.NAMEPREFIX+"early_furnace", EarlyFurnaceInit.earlyFurnace.get()));

        loottable.put(EarlyAlloyFurnaceInit.earlyAlloyFurnace.get(),
                loot.createStandardTable(MetalAgeInit.NAMEPREFIX+"early_alloy_furnace", EarlyAlloyFurnaceInit.earlyAlloyFurnace.get()));



    }
}
