package de.unbekannt.historyTechMod.datagen.loottables;

import com.google.gson.JsonObject;
import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.ageless.AgelessInit;
import de.unbekannt.historyTechMod.ageless.loot_modification.ChiseledStoneModifier;
import de.unbekannt.historyTechMod.ageless.loot_modification.DryGrass;
import de.unbekannt.historyTechMod.ageless.loot_modification.GrainModifier;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import net.minecraft.advancements.criterion.ItemPredicate;
import net.minecraft.advancements.criterion.LocationPredicate;
import net.minecraft.advancements.criterion.StatePropertiesPredicate;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.loot.conditions.*;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.world.biome.Biomes;
import net.minecraftforge.common.data.GlobalLootModifierProvider;

public class GlobalLootModifier extends GlobalLootModifierProvider {

    public GlobalLootModifier(DataGenerator gen) {
        super(gen, HistoryTechMod.MODID);
    }

    @Override
    protected void start() {

        JsonObject raining = new JsonObject();
        raining.addProperty("raining", false);
        raining.addProperty("thundering", false);

        add("dry_grass", AgelessInit.DRY_GRASS_SERIALIZER.get(), new DryGrass(
                new ILootCondition[]{
                        MatchTool.builder(ItemPredicate.Builder.create().item(ItemStack.EMPTY.getItem())).build(),
                        Alternative.builder(
                                BlockStateProperty.builder(Blocks.GRASS),
                                BlockStateProperty.builder(Blocks.TALL_GRASS)
                        ).build(),
                        new WeatherCheck.Serializer().deserialize(raining, null),
                        RandomChance.builder(0.3f).build()
                }
        ));

        add("dry_grass_desert", AgelessInit.DRY_GRASS_SERIALIZER.get(), new DryGrass(
                new ILootCondition[]{
                        Alternative.builder(
                                LocationCheck.builder(LocationPredicate.Builder.builder().biome(Biomes.DESERT)),
                                LocationCheck.builder(LocationPredicate.Builder.builder().biome(Biomes.DESERT_HILLS)),
                                LocationCheck.builder(LocationPredicate.Builder.builder().biome(Biomes.DESERT_LAKES))
                        ).build(),

                        MatchTool.builder(ItemPredicate.Builder.create().item(ItemStack.EMPTY.getItem())).build(),
                        BlockStateProperty.builder(Blocks.DEAD_BUSH).build(),
                        new WeatherCheck.Serializer().deserialize(raining, null),
                        RandomChance.builder(0.5f).build()
                }
        ));

        add("grain", AgelessInit.GRAIN_SERIALIZER.get(), new GrainModifier(
                new ILootCondition[]{
                        MatchTool.builder(ItemPredicate.Builder.create().item(SedentismInit.FLAIL.get())).build(),
                        BlockStateProperty.builder(Blocks.WHEAT)
                                .fromProperties(
                                        StatePropertiesPredicate.Builder
                                                .newBuilder()
                                                .withIntProp(
                                                        BlockStateProperties.AGE_0_7,
                                                        7))
                                .build()
                }, 1, SedentismInit.GRAIN.get(), Items.WHEAT
        ));

        add("chiseled_stone", AgelessInit.CHISELED_STONE_SERIALIZER.get(), new ChiseledStoneModifier(
                new ILootCondition[]{
                        MatchTool.builder(ItemPredicate.Builder.create().item(SedentismInit.STONE_HAMMER.get())).build(),
                        BlockStateProperty.builder(SedentismInit.CHISELEDBLOCK.get()).build()
                },
                Items.STONE, 1, true
        ));


    }
}
