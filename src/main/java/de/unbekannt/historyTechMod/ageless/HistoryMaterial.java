package de.unbekannt.historyTechMod.ageless;

import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;

public class HistoryMaterial {
    public static final Material COPPER = new Material(MaterialColor.STONE,
            false, true, false, false,
            false, false, PushReaction.BLOCK);
}
