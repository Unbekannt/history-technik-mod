package de.unbekannt.historyTechMod.ageless.material;

import ressources.IResources;

public enum Materials implements IResources {
    ;

    private double specificElectricalResistance;
    private double temperatureCoefficient;

    @Override
    public double getSpecificElectricalResistance() {
        return specificElectricalResistance;
    }

    @Override
    public double getTemperatureCoefficient() {
        return temperatureCoefficient;
    }

    Materials(double specificElectricalResistance, double temperatureCoefficient){
        this.specificElectricalResistance = specificElectricalResistance;
        this.temperatureCoefficient = temperatureCoefficient;
    }
}
