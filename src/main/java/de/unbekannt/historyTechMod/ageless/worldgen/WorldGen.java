package de.unbekannt.historyTechMod.ageless.worldgen;

import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;

import static de.unbekannt.historyTechMod.ageless.utils.Utils.RL;

public class WorldGen {

    public static ConfiguredFeature<?, ?> COPPER_ORE;
    public static ConfiguredFeature<?, ?> TIN_ORE;

    public static void addConfigFeatures(RegistryEvent.Register<Feature<?>> event){
        Registry<ConfiguredFeature<?, ?>> registry = WorldGenRegistries.CONFIGURED_FEATURE;
        COPPER_ORE = Feature.ORE.withConfiguration(
                new OreFeatureConfig(OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD, MetalAgeInit.COPPER_ORE.get().getDefaultState(), 10))
                .range(64).square();
        TIN_ORE = Feature.ORE.withConfiguration(
                new OreFeatureConfig(OreFeatureConfig.FillerBlockType.BASE_STONE_OVERWORLD, MetalAgeInit.TIN_ORE.get().getDefaultState(), 10))
                .range(64).square();

        Registry.register(registry, RL("copper_ore"), COPPER_ORE);
        Registry.register(registry, RL("tin_ore"), TIN_ORE);
    }
    
    public static void handleWorldGen(BiomeLoadingEvent event){
        RegistryKey<Biome> biome = RegistryKey.getOrCreateKey(Registry.BIOME_KEY, event.getName());
        if (event.getCategory() == Biome.Category.NETHER
                || event.getCategory() == Biome.Category.THEEND
                || BiomeDictionary.hasType(biome, BiomeDictionary.Type.VOID)) return;
        event.getGeneration().withFeature(GenerationStage.Decoration.UNDERGROUND_ORES, COPPER_ORE);
        event.getGeneration().withFeature(GenerationStage.Decoration.UNDERGROUND_ORES, TIN_ORE);
    }
}
