package de.unbekannt.historyTechMod.ageless;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

import java.util.function.Supplier;

public class ModItemGroups{

    public static final ItemGroup SEDENTISMGROUP = new ModItemGroup("sedemtism",
            () -> new ItemStack(SedentismInit.STONE_HAMMER.get()));

    public static final ItemGroup METALAGEGROUP = new ModItemGroup("metalage",
            () -> new ItemStack(MetalAgeInit.COPPER_ORE_ITEM.get()));

    public static class ModItemGroup extends ItemGroup {

        private final Supplier<ItemStack> iconSupplier;

        public ModItemGroup(final String name, final Supplier<ItemStack> supplier) {
            super(HistoryTechMod.MODID+"."+name);
            iconSupplier = supplier;
        }
        
        @Override
        public ItemStack createIcon() {
            return iconSupplier.get();
        }
    }
}


