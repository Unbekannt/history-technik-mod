package de.unbekannt.historyTechMod.ageless;

import de.unbekannt.historyTechMod.metalage.MetalAgeInit;
import net.minecraft.item.IItemTier;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.LazyValue;

import java.util.function.Supplier;

public enum ItemTiers implements IItemTier {

    COPPER(150, 5.0f, 1.5f, 1, 7, () -> Ingredient.fromItems(MetalAgeInit.COPPER_ORE_ITEM.get()));

    private final int harvestLevel;
    private final int maxUses;
    private final float efficiency;
    private final float attackDamage;
    private final int enchantAbility;
    private final LazyValue<Ingredient> repairMaterial;

    ItemTiers(int maxUses, float efficiency, float attackDamage, int harvestLevel, int enchantAbility, Supplier<Ingredient> repairMaterial){
        this.maxUses = maxUses;
        this.efficiency = efficiency;
        this.attackDamage = attackDamage;
        this.harvestLevel = harvestLevel;
        this.enchantAbility = enchantAbility;
        this.repairMaterial = new LazyValue<>(repairMaterial);
    }

    @Override
    public int getMaxUses() {
        return maxUses;
    }

    @Override
    public float getEfficiency() {
        return efficiency;
    }

    @Override
    public float getAttackDamage() {
        return attackDamage;
    }

    @Override
    public int getHarvestLevel() {
        return harvestLevel;
    }

    @Override
    public int getEnchantability() {
        return enchantAbility;
    }

    @Override
    public Ingredient getRepairMaterial() {
        return repairMaterial.getValue();
    }
}
