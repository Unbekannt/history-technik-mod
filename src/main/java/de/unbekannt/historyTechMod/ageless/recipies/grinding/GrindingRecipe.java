package de.unbekannt.historyTechMod.ageless.recipies.grinding;

import de.unbekannt.historyTechMod.ageless.AgelessInit;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.items.wrapper.RecipeWrapper;

public class GrindingRecipe implements IGrindingRecipe {

    private final ResourceLocation id;
    private Ingredient input;
    private final ItemStack output;
    private int timer;

    public GrindingRecipe(ResourceLocation id, Ingredient input, ItemStack output, int timer){
        this.id = id;
        this.input = input;
        this.output = output;
        this.timer = timer;
    }

    @Override
    public Ingredient getInput() {

        return input;
    }

    @Override
    public int getTimer() {
        return timer;
    }

    @Override
    public boolean matches(RecipeWrapper inv, World worldIn) {
        return this.input.test(inv.getStackInSlot(0));
    }

    @Override
    public ItemStack getCraftingResult(RecipeWrapper inv) {
        return output.copy();
    }

    @Override
    public ItemStack getRecipeOutput() {
        return output.copy();
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return AgelessInit.GRINDING.get();
    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        return NonNullList.from(Ingredient.fromItems(ItemStack.EMPTY.getItem()), this.input);
    }
}
