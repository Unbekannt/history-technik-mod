package de.unbekannt.historyTechMod.ageless.recipies.earlyFurnace;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.ageless.AgelessInit;
import de.unbekannt.historyTechMod.ageless.recipies.common.ICommonRecipe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.items.wrapper.RecipeWrapper;
import org.jetbrains.annotations.NotNull;

public class EarlyFurnaceRecipe implements ICommonRecipe {

    public static final ResourceLocation RECIPE_TYPE_ID = new ResourceLocation(HistoryTechMod.MODID, "early_furnace");

    private final ResourceLocation id;
    private Ingredient input;
    private final ItemStack output;
    private int cookTime;

    public EarlyFurnaceRecipe(ResourceLocation id, Ingredient input, ItemStack output, int time){
        this.id = id;
        this.input = input;
        this.output = output;
        this.cookTime = time;
    }

    @Override
    public boolean matches(RecipeWrapper inv, World worldIn) {
        return this.input.test(inv.getStackInSlot(0));
    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        return NonNullList.from(Ingredient.fromItems(ItemStack.EMPTY.getItem()), this.input);
    }

    @Override
    public ItemStack getCraftingResult(RecipeWrapper inv) {
        return output.copy();
    }

    @Override
    public ItemStack getRecipeOutput() {
        return output.copy();
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return AgelessInit.EARLY_FURNACE.get();
    }

    @Override
    public int getTimer() {
        return cookTime;
    }

    @NotNull
    @Override
    public ResourceLocation getTypeId() {
        return RECIPE_TYPE_ID;
    }
}
