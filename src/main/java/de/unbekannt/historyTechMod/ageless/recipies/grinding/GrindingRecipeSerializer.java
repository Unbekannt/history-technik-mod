package de.unbekannt.historyTechMod.ageless.recipies.grinding;

import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;

public class GrindingRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<GrindingRecipe> {
    @Override
    public GrindingRecipe read(ResourceLocation recipeId, JsonObject json) {
        ItemStack output = CraftingHelper.getItemStack(json.getAsJsonObject("result"), true);
        Ingredient input = Ingredient.deserialize(JSONUtils.getJsonObject(json, "ingredient"));
        int time = JSONUtils.getInt(json, "grindTime");
        return new GrindingRecipe(recipeId, input, output, time);
    }

    @Nullable
    @Override
    public GrindingRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
        ItemStack output = buffer.readItemStack();
        Ingredient input = Ingredient.read(buffer);
        int timer = buffer.readVarInt();
        return new GrindingRecipe(recipeId, input, output, timer);
    }

    @Override
    public void write(PacketBuffer buffer, GrindingRecipe recipe) {
        Ingredient input = recipe.getIngredients().get(0);
        input.write(buffer);
        buffer.writeVarInt(recipe.getTimer());

        buffer.writeItemStack(recipe.getRecipeOutput());
    }
}
