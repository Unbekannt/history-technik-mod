package de.unbekannt.historyTechMod.ageless.recipies.earlyFurnace;

import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;

public class EarlyFurnaceRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<EarlyFurnaceRecipe> {


    @Override
    public EarlyFurnaceRecipe read(ResourceLocation recipeId, JsonObject json) {
        ItemStack output = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(JSONUtils.getString(json, "result"))));
        Ingredient input = Ingredient.deserialize(JSONUtils.getJsonObject(json, "ingredient"));
        int time = JSONUtils.getInt(json, "cookingtime");
        return new EarlyFurnaceRecipe(recipeId, input, output, time);
    }

    @Nullable
    @Override
    public EarlyFurnaceRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
        ItemStack output = buffer.readItemStack();
        Ingredient input = Ingredient.read(buffer);
        int time = buffer.readInt();
        return new EarlyFurnaceRecipe(recipeId, input, output, time);
    }

    @Override
    public void write(PacketBuffer buffer, EarlyFurnaceRecipe recipe) {
        Ingredient input = recipe.getIngredients().get(0);
        input.write(buffer);
        buffer.writeInt(recipe.getTimer());
        buffer.writeItemStack(recipe.getRecipeOutput());
    }
}
