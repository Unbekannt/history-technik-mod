package de.unbekannt.historyTechMod.ageless.recipies;

import com.google.gson.JsonObject;
import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.ageless.config.Config;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.conditions.ICondition;
import net.minecraftforge.common.crafting.conditions.IConditionSerializer;

public class ConfigCondition implements ICondition {

    private static final ResourceLocation NAME = new ResourceLocation(HistoryTechMod.MODID, "config");

    private boolean condition;
    private String configName;

    public ConfigCondition(String configName) {
        this(true, configName);
    }

    public ConfigCondition(boolean use_condition, String name) {
        this.condition = use_condition;
        this.configName = name;
    }

    @Override
    public ResourceLocation getID() {
        return NAME;
    }

    @Override
    public boolean test() {
        return ((Boolean)Config.lookupTable.get(configName).get()) == condition;
    }

    public static class Serializer implements IConditionSerializer<ConfigCondition> {

        public static final Serializer INSTANCE = new Serializer();

        @Override
        public void write(JsonObject json, ConfigCondition value) {
            json.addProperty("condition", value.condition);
            json.addProperty("configName", value.configName);
        }

        @Override
        public ConfigCondition read(JsonObject json) {
            boolean condition = json.get("condition").getAsBoolean();
            String name = json.get("configName").getAsString();
            return new ConfigCondition(condition, name);
        }

        @Override
        public ResourceLocation getID() {
            return NAME;
        }
    }
}
