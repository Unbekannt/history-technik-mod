package de.unbekannt.historyTechMod.ageless.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.common.Mod;

import java.util.HashMap;

@Mod.EventBusSubscriber
public class Config {

    public static final String CATEGORY_SEDENTISM = "sendentism";
    public static final String SUBCATEGORY_QUERN = "quern";
    public static final String SUBCATEGORY_CRAFTING = "crafting";

    public static final HashMap<String, ForgeConfigSpec.ConfigValue<?>> lookupTable = new HashMap<>();

    public static ForgeConfigSpec SERVER_CONFIG;
    public static ForgeConfigSpec CLIENT_CONFIG;
    public static ForgeConfigSpec COMMON_CONFIG;

    public static ForgeConfigSpec.IntValue QUERN_TICK_DIVIDER;
    public static ForgeConfigSpec.BooleanValue USE_ALTERNATIVE_FURNACE;



    static {
        ForgeConfigSpec.Builder SERVER_BUILDER = new ForgeConfigSpec.Builder();
        ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();
        ForgeConfigSpec.Builder COMMON_BUILDER = new ForgeConfigSpec.Builder();

        //Define Settings and stuff
        SERVER_BUILDER.comment("Sedentism configs").push(CATEGORY_SEDENTISM);

        setupServerQuernConfig(SERVER_BUILDER);

        SERVER_BUILDER.pop();


        COMMON_BUILDER.comment("Sedentism config").push(CATEGORY_SEDENTISM);

        setupCommonQuernConfig(COMMON_BUILDER);

        COMMON_BUILDER.pop();

        SERVER_CONFIG = SERVER_BUILDER.build();
        COMMON_CONFIG = COMMON_BUILDER.build();
        CLIENT_CONFIG = CLIENT_BUILDER.build();
    }

    private static void setupCommonQuernConfig(ForgeConfigSpec.Builder common_builder) {
        common_builder.comment("Recipes").push(SUBCATEGORY_CRAFTING);
        USE_ALTERNATIVE_FURNACE = common_builder.comment(
                "use the alternative Furnace path, makes the default furnaces only available after some time in " +
                        "the Metalage, condition key: alternative_Recipe")
                .define("alternative_Recipe", true);
        lookupTable.put("alternative_Recipe", USE_ALTERNATIVE_FURNACE);
        common_builder.pop();
    }

    private static void setupServerQuernConfig(ForgeConfigSpec.Builder server_builder) {
        server_builder.comment("Quern settings").push(SUBCATEGORY_QUERN);

        QUERN_TICK_DIVIDER = server_builder.comment("Divider for the grindTime, condition key: divider")
                .defineInRange("divider", 10, 0, 10);
        lookupTable.put("divider", QUERN_TICK_DIVIDER);
        server_builder.pop();

    }


}
