package de.unbekannt.historyTechMod.ageless.loot_modification;

import com.google.gson.JsonObject;
import de.unbekannt.historyTechMod.sedentism.SedentismInit;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.conditions.ILootCondition;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DryGrass extends LootModifier {

    /**
     * Constructs a LootModifier.
     *
     * @param conditionsIn the ILootConditions that need to be matched before the loot is modified.
     */
    public DryGrass(ILootCondition[] conditionsIn) {
        super(conditionsIn);
    }

    @NotNull
    @Override
    protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
        generatedLoot.add(new ItemStack(SedentismInit.DRY_GRASS.get()));
        return generatedLoot;
    }

    public static class Serializer extends GlobalLootModifierSerializer<DryGrass>{

        @Override
        public DryGrass read(ResourceLocation location, JsonObject object, ILootCondition[] ailootcondition) {
            return new DryGrass(ailootcondition);
        }

        @Override
        public JsonObject write(DryGrass instance) {
            return makeConditions(instance.conditions);
        }
    }

}
