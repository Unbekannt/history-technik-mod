package de.unbekannt.historyTechMod.ageless.loot_modification;

import com.google.gson.JsonObject;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.conditions.ILootCondition;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;

public class GrainModifier extends LootModifier {
    private final int numSeedToConvert;
    private final Item reward;
    private Item itemToCheck;

    /**
     * Constructs a LootModifier.
     * @param conditionsIn the ILootConditions that need to be matched before the loot is modified.
     * @param sumSeeds
     * @param output
     * @param seed
     */
    public GrainModifier(ILootCondition[] conditionsIn, int sumSeeds, Item output, Item seed) {
        super(conditionsIn);
        numSeedToConvert = sumSeeds;
        reward = output;
        itemToCheck = seed;
    }

    @Override
    protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
        generatedLoot.removeIf(x -> x.getItem() == itemToCheck);
        generatedLoot.add(new ItemStack(reward, numSeedToConvert));
        return generatedLoot;
    }

    public static class Serializer extends GlobalLootModifierSerializer<GrainModifier> {

        @Override
        public GrainModifier read(ResourceLocation location, JsonObject object, ILootCondition[] ailootcondition) {
            int sumSeeds = JSONUtils.getInt(object, "numSeeds");
            Item output = ForgeRegistries.ITEMS.getValue(new ResourceLocation(JSONUtils.getString(object, "output")));
            Item seed = ForgeRegistries.ITEMS.getValue(new ResourceLocation(JSONUtils.getString(object, "seedItem")));
            return new GrainModifier(ailootcondition, sumSeeds, output, seed);
        }

        @Override
        public JsonObject write(GrainModifier instance) {
            JsonObject json = makeConditions(instance.conditions);
            json.addProperty("numSeeds", instance.numSeedToConvert);
            json.addProperty("output", ForgeRegistries.ITEMS.getKey(instance.reward).toString());
            json.addProperty("seedItem", ForgeRegistries.ITEMS.getKey(instance.itemToCheck).toString());
            return json;
        }
    }
}


