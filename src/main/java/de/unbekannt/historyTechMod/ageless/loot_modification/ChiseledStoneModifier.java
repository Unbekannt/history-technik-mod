package de.unbekannt.historyTechMod.ageless.loot_modification;

import com.google.gson.JsonObject;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootContext;
import net.minecraft.loot.conditions.ILootCondition;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;

public class ChiseledStoneModifier extends LootModifier {

    private final Item output;
    private final int volume;
    private final boolean removeOther;
    /**
     * Constructs a LootModifier.
     *  @param conditionsIn the ILootConditions that need to be matched before the loot is modified.
     * @param output
     * @param volume
     * @param removeOther
     */
    public ChiseledStoneModifier(ILootCondition[] conditionsIn, Item output, int volume, boolean removeOther) {
        super(conditionsIn);
        this.output = output;
        this.volume = volume;
        this.removeOther = removeOther;
    }

    @Override
    protected List<ItemStack> doApply(List<ItemStack> generatedLoot, LootContext context) {
        if(removeOther) generatedLoot.clear();
        //generatedLoot.removeIf(itemStack -> removeOther);
        generatedLoot.add(new ItemStack(output, volume));
        return generatedLoot;
    }

    public static class Serializer extends GlobalLootModifierSerializer<ChiseledStoneModifier>{

        @Override
        public ChiseledStoneModifier read(ResourceLocation location, JsonObject object, ILootCondition[] ailootcondition) {
            int volume = JSONUtils.getInt(object, "numOutput");
            Item output = ForgeRegistries.ITEMS.getValue(new ResourceLocation(JSONUtils.getString(object, "output")));
            boolean removeOther = JSONUtils.getBoolean(object, "removeOtherItems");
            return new ChiseledStoneModifier(ailootcondition, output, volume, removeOther);
        }

        @Override
        public JsonObject write(ChiseledStoneModifier instance) {
            JsonObject json = makeConditions(instance.conditions);
            json.addProperty("numOutput", instance.volume);
            json.addProperty("output", ForgeRegistries.ITEMS.getKey(instance.output).toString());
            json.addProperty("removeOtherItems", instance.removeOther);
            return json;
        }
    }
}
