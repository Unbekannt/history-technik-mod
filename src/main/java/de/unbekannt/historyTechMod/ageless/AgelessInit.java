package de.unbekannt.historyTechMod.ageless;

import de.unbekannt.historyTechMod.HistoryTechMod;
import de.unbekannt.historyTechMod.ageless.config.Config;
import de.unbekannt.historyTechMod.ageless.loot_modification.ChiseledStoneModifier;
import de.unbekannt.historyTechMod.ageless.loot_modification.DryGrass;
import de.unbekannt.historyTechMod.ageless.loot_modification.GrainModifier;
import de.unbekannt.historyTechMod.ageless.recipies.ConfigCondition;
import de.unbekannt.historyTechMod.ageless.recipies.alloyRecipe.AlloyRecipe;
import de.unbekannt.historyTechMod.ageless.recipies.alloyRecipe.AlloyRecipeSerializer;
import de.unbekannt.historyTechMod.ageless.recipies.earlyFurnace.EarlyFurnaceRecipe;
import de.unbekannt.historyTechMod.ageless.recipies.earlyFurnace.EarlyFurnaceRecipeSerializer;
import de.unbekannt.historyTechMod.ageless.recipies.grinding.GrindingRecipeSerializer;
import de.unbekannt.historyTechMod.ageless.recipies.grinding.IGrindingRecipe;
import de.unbekannt.historyTechMod.ageless.worldgen.WorldGen;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.gen.feature.Feature;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.crafting.CraftingHelper;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import static net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@EventBusSubscriber(modid = HistoryTechMod.MODID, bus = Bus.MOD)
public class AgelessInit {

    public static final IRecipeType<IGrindingRecipe> GRINDING_TYPE = registerType(IGrindingRecipe.RECIPE_TYPE_ID);

    public static final IRecipeType<EarlyFurnaceRecipe> EARLY_FURNACE_TYPE = registerType(EarlyFurnaceRecipe.RECIPE_TYPE_ID);

    public static final IRecipeType<AlloyRecipe> ALLOY_RECIPE_RECIPE_TYPE = registerType(AlloyRecipe.ALLOY_FURNACE);

    public static final DeferredRegister<IRecipeSerializer<?>> RECIPE_SERIALIZER = DeferredRegister.create(
            ForgeRegistries.RECIPE_SERIALIZERS, HistoryTechMod.MODID
    );

    public static final DeferredRegister<GlobalLootModifierSerializer<?>> LOOT_MODIFIER = DeferredRegister.create(
            ForgeRegistries.LOOT_MODIFIER_SERIALIZERS, HistoryTechMod.MODID
    );

    public static final DeferredRegister<Feature<?>> worldGenFeature = DeferredRegister.create(ForgeRegistries.FEATURES, HistoryTechMod.MODID);

    public static final RegistryObject<GrindingRecipeSerializer> GRINDING = RECIPE_SERIALIZER.register("grinding", GrindingRecipeSerializer::new);

    public static final RegistryObject<EarlyFurnaceRecipeSerializer> EARLY_FURNACE = RECIPE_SERIALIZER.register("early_furnace", EarlyFurnaceRecipeSerializer::new);

    public static final RegistryObject<AlloyRecipeSerializer> ALLOY_RECIPE = RECIPE_SERIALIZER.register("alloy_recipe", AlloyRecipeSerializer::new);

    public static final RegistryObject<GrainModifier.Serializer> GRAIN_SERIALIZER = LOOT_MODIFIER.register("grain", GrainModifier.Serializer::new);

    public static final RegistryObject<ChiseledStoneModifier.Serializer> CHISELED_STONE_SERIALIZER = LOOT_MODIFIER.register("chiseled_stone", ChiseledStoneModifier.Serializer::new);

    public static final RegistryObject<DryGrass.Serializer> DRY_GRASS_SERIALIZER = LOOT_MODIFIER.register("dry_grass", DryGrass.Serializer::new);



    public static void init(){
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, Config.CLIENT_CONFIG);
        ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, Config.SERVER_CONFIG);
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, Config.COMMON_CONFIG);

        FMLJavaModLoadingContext.get().getModEventBus().addGenericListener(Feature.class, EventPriority.LOW, WorldGen::addConfigFeatures);
        MinecraftForge.EVENT_BUS.addListener(WorldGen::handleWorldGen);

        RECIPE_SERIALIZER.register(FMLJavaModLoadingContext.get().getModEventBus());
        LOOT_MODIFIER.register(FMLJavaModLoadingContext.get().getModEventBus());




    }


    @SuppressWarnings("unchecked")
    private static <T extends IRecipeType<?>> T registerType(ResourceLocation recipeTypeId) {
        return (T) Registry.register(Registry.RECIPE_TYPE, recipeTypeId, new RegisterType<>());
    }

    private static class RegisterType<T extends IRecipe<?>> implements IRecipeType<T> {
        @Override
        public String toString() {
            return Registry.RECIPE_TYPE.getKey(this).toString();
        }
    }

    @SubscribeEvent
    public static void addRecipeSerializer(RegistryEvent.Register<IRecipeSerializer<?>> handler){
        CraftingHelper.register(ConfigCondition.Serializer.INSTANCE);
    }

}
