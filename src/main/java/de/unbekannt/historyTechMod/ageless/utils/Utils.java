package de.unbekannt.historyTechMod.ageless.utils;

import de.unbekannt.historyTechMod.HistoryTechMod;
import net.minecraft.util.ResourceLocation;

public class Utils {
    public static ResourceLocation RL(String name){
        return new ResourceLocation(HistoryTechMod.MODID, name);
    }
}
